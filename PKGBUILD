# Maintainer: Jan de Groot <jgc@archlinux.org>
# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Arjan Timmerman <arjan@archlinux.org>
# Contributor: Wael Nasreddine <gandalf@siemens-mobiles.org>
# Contributor: Tor Krill <tor@krill.nu>
# Contributor: Will Rea <sillywilly@gmail.com>

pkgbase=network-manager-applet
pkgname=(network-manager-applet nm-connection-editor libnma)
pkgdesc="Applet for managing network connections"
url="https://wiki.gnome.org/Projects/NetworkManager/"
pkgver=1.8.22
pkgrel=1.1
arch=(x86_64)
license=(GPL LGPL)
makedepends=(libsecret libnotify libmm-glib intltool gobject-introspection git gtk-doc meson
             libnm gcr gtk3 iso-codes mobile-broadband-provider-info)
_commit=b9f22d35aa2c6356e29e2e646298797ad90f5bdc  # tags/1.8.22^0
source=("git+https://gitlab.gnome.org/GNOME/network-manager-applet.git#commit=$_commit"
        https://gitlab.gnome.org/lkundrak/network-manager-applet/commit/3bbb4523.patch
        https://gitlab.gnome.org/lkundrak/network-manager-applet/commit/5c44ffc5.patch)
sha256sums=('SKIP'
            '085ad39e6697878cc78410ad1c811b20f7542fa69ae09cfa0afa4cf0c4d7a373'
            'd5aa5e4e0bf4f95c895b16d31d8d24c3bd7fb606fdac60fcbeb97aa707eb354f')

pkgver() {
  cd $pkgbase
  git describe --tags | sed 's/-dev/dev/;s/-/+/g'
}

prepare() {
  cd $pkgbase
  patch -Rp1 -i $srcdir/3bbb4523.patch
  patch -Rp1 -i $srcdir/5c44ffc5.patch
}

build() {
  arch-meson $pkgbase build -D selinux=false
  ninja -C build
}

check() {
  meson test -C build
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_network-manager-applet() {
  depends=(nm-connection-editor libmm-glib libnotify libsecret networkmanager)

  DESTDIR="$pkgdir" meson install -C build

### Split libnma
  _pick libnma "$pkgdir"/usr/include/libnma
  _pick libnma "$pkgdir"/usr/lib/girepository-1.0/NMA-*
  _pick libnma "$pkgdir"/usr/lib/libnma.*
  _pick libnma "$pkgdir"/usr/lib/pkgconfig/libnma.pc
  _pick libnma "$pkgdir"/usr/share/gir-1.0/NMA-*
  _pick libnma "$pkgdir"/usr/share/glib-2.0/schemas
  _pick libnma "$pkgdir"/usr/share/gtk-doc/html/libnma

### Split nm-connection-editor
  _pick nm-connection-editor "$pkgdir"/usr/bin/nm-connection-editor
  _pick nm-connection-editor "$pkgdir"/usr/share/applications/nm-connection-editor.desktop
  _pick nm-connection-editor "$pkgdir"/usr/share/icons/hicolor/22x22/apps/nm-device-wwan.png
  _pick nm-connection-editor "$pkgdir"/usr/share/locale
  _pick nm-connection-editor "$pkgdir"/usr/share/man/man1/nm-connection-editor.1
  _pick nm-connection-editor "$pkgdir"/usr/share/metainfo
}

package_nm-connection-editor() {
  pkgdesc="NetworkManager GUI connection editor and widgets"
  depends=(libnma)
  conflicts=('libnm-gtk<1.8.18-1')
  replaces=('libnm-gtk<1.8.18-1')
  mv nm-connection-editor/* "$pkgdir"
}

package_libnma() {
  pkgdesc="NetworkManager GUI client library"
  depends=(libnm gcr gtk3 iso-codes mobile-broadband-provider-info)
  mv libnma/* "$pkgdir"
}